package com.example.rebussearch.model

import androidx.annotation.ColorInt

data class Inventory(val startTime : String, val duration : Long, val reachesLocationIn : Int, val rating : Float,
                     val nosRating : Int, val seats: Seats, val bus: Bus, val ratingColor : Int, val endTime : String)

data class Seats(val seatsRemaining: Int, val baseFare : Int)

data class Bus(val travelsName: String, val regno : String, val type : String?)

data class BusInfoModel(val inventory : List<Inventory>)