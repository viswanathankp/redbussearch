package com.example.rebussearch

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.rebussearch.model.BusInfoModel
import kotlinx.android.synthetic.main.activity_main.*

class SearchActivity : AppCompatActivity() {

    private val vm: SearchViewModel by lazy {
        getViewModel { SearchViewModel(SearchActivity@this.application, getSearchUseCase) }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar.let {
            it?.setHomeButtonEnabled(true)
            it?.setDisplayHomeAsUpEnabled(true)
            it?.title = getString(R.string.bus_title)
        }
        recycler_view.layoutManager = LinearLayoutManager(this)
    }

    private fun initRecyclerView(data : BusInfoModel){
        recycler_view.adapter = getBusListAdapter(data)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        vm.fetchBusList().observe(this,
            Observer { t ->
                progress_container.visibility = GONE
                if(t == null){
                    error_view.visibility = VISIBLE
                    recycler_view.visibility = GONE
                } else {
                    error_view.visibility = GONE
                    recycler_view.visibility = VISIBLE
                    initRecyclerView(t)
                }
            })
    }
}
