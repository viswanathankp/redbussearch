package com.example.rebussearch

const val BASE_URL: String = "https://st.redbus.in"
const val SERVER_TIME_FORMAT : String = "yyyy-MM-dd'T'HH:mm:ss"
const val DISPLAY_TIME_FORMAT : String = "hh:mm a"