package com.example.rebussearch

import android.app.Application
import androidx.annotation.NonNull
import androidx.lifecycle.*
import androidx.lifecycle.Observer
import com.example.rebussearch.model.Bus
import com.example.rebussearch.model.BusInfoModel
import com.example.rebussearch.model.Inventory
import com.example.rebussearch.model.Seats
import com.example.rebussearch.network.schema.BusListSchema
import java.text.SimpleDateFormat
import java.util.*

class SearchViewModel(@NonNull val app: Application, private val searchUseCase: SearchUseCase) : AndroidViewModel(app),
    LifecycleOwner {

    private var lifecycleRegistry: LifecycleRegistry = LifecycleRegistry(this)

    override fun getLifecycle(): Lifecycle {
        return lifecycleRegistry
    }

    init {
        lifecycleRegistry.markState(Lifecycle.State.STARTED)
    }

    private val modelData: MutableLiveData<BusInfoModel> = MutableLiveData()

    fun fetchBusList(): LiveData<BusInfoModel> {
        searchUseCase.fetchBusList().observe(this,
            Observer { t -> modelData.value = getBusInfoModel(t) })
        return modelData
    }

    private fun getBusInfoModel(busListSchema: BusListSchema?): BusInfoModel? {

        if (busListSchema == null) return null

        val inventoryList: MutableList<Inventory> = mutableListOf()

        for (item: com.example.rebussearch.network.schema.Inventory in busListSchema.inventory) {
            val bus = Bus(
                busListSchema.travels.getValue(item.bus.travelsName.toString()),
                item.bus.regno,
                busListSchema.busType.getValue(item.bus.type.toString())
            )
            val seat = Seats(item.seats.seatsRemaining, item.seats.baseFare)
            val invent = Inventory(
                getStartTime(item.startTime),
                item.duration,
                getReachTime(item.reachesLocationIn),
                item.rating,
                item.nosRating,
                seat,
                bus,
                getColorInt(item.rating),
                getEndTime(item.startTime, item.duration.toInt())
            )
            inventoryList.add(invent)
        }

        return BusInfoModel(inventoryList)
    }

    private fun getReachTime(time : Int): Int{
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.MINUTE, time)
        return calendar.get(Calendar.MINUTE)
    }

    private fun getStartTime(time: String): String{
        val parser = SimpleDateFormat(SERVER_TIME_FORMAT, Locale.ENGLISH)
        val simpleFormatter = SimpleDateFormat(DISPLAY_TIME_FORMAT, Locale.ENGLISH)
        return simpleFormatter.format(parser.parse(time))
    }

    private fun getEndTime(time: String, duration : Int): String{
        val parser = SimpleDateFormat(SERVER_TIME_FORMAT, Locale.ENGLISH)
        val simpleFormatter = SimpleDateFormat(DISPLAY_TIME_FORMAT, Locale.ENGLISH)
        simpleFormatter.format(parser.parse(time))
        simpleFormatter.calendar.add(Calendar.MINUTE, duration)
        return simpleFormatter.format(simpleFormatter.calendar.time)
    }

    private fun getColorInt(rating: Float): Int {
        return if (rating > 4.0) R.color.color_green
        else R.color.color_orange
    }

    override fun onCleared() {
        super.onCleared()
        lifecycleRegistry.markState(Lifecycle.State.DESTROYED)
    }
}