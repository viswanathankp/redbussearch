package com.example.rebussearch

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.rebussearch.network.SearchApi
import com.example.rebussearch.network.schema.BusListSchema
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SearchUseCase(private val searchApi: SearchApi) {

    val responseData : MutableLiveData<BusListSchema?> = MutableLiveData()

    fun fetchBusList() : LiveData<BusListSchema?> {
        searchApi.fetchBusList().enqueue(object : Callback<BusListSchema>{
            override fun onFailure(call: Call<BusListSchema>, t: Throwable) {
                responseData.value = null
            }

            override fun onResponse(call: Call<BusListSchema>, response: Response<BusListSchema>) {
                responseData.value = response.body()
            }
        })
        return responseData
    }
}