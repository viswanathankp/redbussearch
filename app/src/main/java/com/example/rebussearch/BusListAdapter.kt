package com.example.rebussearch

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.rebussearch.model.BusInfoModel
import com.example.rebussearch.model.Inventory
import kotlinx.android.synthetic.main.layout_list_item.view.*
import kotlinx.android.synthetic.main.layout_time_line.view.*

class BusListAdapter(data: BusInfoModel) : RecyclerView.Adapter<BusListAdapter.BusItemViewHolder>() {

    private val list: List<Inventory> = data.inventory

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BusItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return BusItemViewHolder(inflater, parent)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: BusItemViewHolder, position: Int) {
        val item: Inventory = list[position]
        holder.bind(item)
    }

    class BusItemViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.layout_list_item, parent, false)) {

        fun bind(inventory: Inventory) {
            itemView.time.text = "%s mins".format(inventory.reachesLocationIn)
            itemView.rating.text = inventory.rating.toString()
            itemView.rating.setBackgroundColor(itemView.resources.getColor(inventory.ratingColor))
            itemView.rating_count.text = "%s ratings".format(inventory.nosRating.toString())
            itemView.seats_left.text = "%s seats left | ".format(inventory.seats.seatsRemaining.toString())
            itemView.price.text = "\u20B9%s".format(inventory.seats.baseFare.toString())
            itemView.start_time.text = inventory.startTime
            itemView.end_time.text = inventory.endTime
            itemView.travels_name.text = inventory.bus.travelsName
            itemView.info.text = "%s | %s".format(inventory.bus.regno, inventory.bus.type)
        }
    }
}