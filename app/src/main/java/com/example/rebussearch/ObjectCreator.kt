package com.example.rebussearch

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import com.example.rebussearch.model.BusInfoModel
import com.example.rebussearch.network.Network
import com.example.rebussearch.network.SearchApi

val getSearchApi: SearchApi = Network.getRetrofit()
    .create(SearchApi::class.java)

val getSearchUseCase : SearchUseCase = SearchUseCase(getSearchApi)

inline fun <reified T : ViewModel> AppCompatActivity.getViewModel(noinline creator: (() -> T)? = null): T {
    return if (creator == null)
        ViewModelProviders.of(this).get(T::class.java)
    else
        ViewModelProviders.of(this, BaseViewModelFactory(creator)).get(T::class.java)
}

fun getBusListAdapter(data : BusInfoModel) : BusListAdapter {
    return BusListAdapter(data)
}