package com.example.rebussearch.network.schema

data class Inventory(val startTime : String, val duration : Long, val reachesLocationIn : Int, val rating : Float,
                     val nosRating : Int, val seats: Seats, val bus: Bus)

data class Seats(val seatsRemaining: Int, val baseFare : Int, val discount : Int)

data class Bus(val travelsName: Int, val regno : String, val type : Int)