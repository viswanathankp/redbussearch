package com.example.rebussearch.network.schema

data class BusListSchema(val inventory: List<Inventory>, val busType: Map<String, String>, val travels: Map<String, String>)