package com.example.rebussearch.network

import com.example.rebussearch.network.schema.BusListSchema
import retrofit2.Call
import retrofit2.http.GET

interface SearchApi {
    @GET("/test/generated.json")
  fun fetchBusList(): Call<BusListSchema>
}